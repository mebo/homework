import kotlin.math.pow
import kotlin.math.sqrt

class Point(
    private var x: Int,
    private var y: Int
) {

    override fun toString(): String {
        return "($x,$y)"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Point) {
            return this.x * other.y == this.y * other.x
        }
        return false
    }

    fun moveByY() {
        y *= -1
    }

    fun moveByX() {
        x *= -1
    }

    fun distanceFrom(other : Point): Double {
        // formula sqrt((x2 - x1)^2 + (y2 - y1)^2)
        return sqrt((other.x - this.x.toDouble()).pow(2) + (other.y - this.y.toDouble()).pow(2))
    }
}